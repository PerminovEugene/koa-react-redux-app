import test from 'ava'
    
// TODO now Ava version is 0.18.2!! there all tests always pass, can't understand why

const validators = require('./../app/validators/validators')
    , _ = require('lodash');

test('async validation test with correct data', t => {
    const data = {foo: "foo", bar: "bar"}
        , expectedResult = undefined
        , config = {
            "foo": {"required": true}
        };
    validators.validate(data, config)
        .then((result) => {
            t.is(result, expectedResult);
        });
});

test('async validation test with not correct data', t => {
    const data = {bar: "bar"}
        , expectedResult = {foo: 'Field foo is required.'}
        , config = {
        "foo": {"required": true}
    };
    validators.validate(data, config)
        .then((result) => {
            t.deepEqual(result, expectedResult);
        });
});
