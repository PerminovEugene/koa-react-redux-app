const models = require('./../models')
    , _ = require('lodash');

const addIncludesToOptions = (options, config, flagName) => {
    if (config.associatedForeignKeys) {
        _.each(config.associatedForeignKeys, (fieldConfig, key) => {
            if (fieldConfig[flagName]) {
                options.include = models[fieldConfig.model];
            }
        });
    }
};

module.exports = {
    create: (body, config) => {
        return new Promise((resolve, reject) => {
            models[config.modelName]
                .create(body)
                .then((item) => {
                    if (config.associatedForeignKeys) {
                        let options = {};
                        addIncludesToOptions(options, config, 'addOnCreate');
                        return resolve(models[config.modelName].findById(item.id, options));
                    }
                    return resolve(item);
                })
                .catch((err) => {
                    reject(err);
                });
        });
    },
    
    read: (id, config) => {
        return new Promise((resolve, reject) => {
            let options = {};
            addIncludesToOptions(options, config, 'addOnRead');
            models[config.modelName]
                .findById(id, options)
                .then((item) => {
                    resolve(item);
                })
                .catch((err) => {
                    reject(err);
                });
        });
    },
    
    readAll: (config) => {
        return new Promise((resolve, reject) => {
    
            let options = {};
            addIncludesToOptions(options, config, 'addOnReadAll');
            
            models[config.modelName]
                .findAll(options)
                .then((items) => {
                    resolve(items);
                })
                .catch((err) => {
                    reject(err);
                });
        });
    },
    
    update: (id, body, config) => {
        return new Promise((resolve, reject) => {
            let options = {};
            addIncludesToOptions(options, config, 'addOnUpdate');
            models[config.modelName]
                .findById(id)
                .then((item) => {
                    if (item === null) {
                        return resolve(null);
                    }
                    return item.update(body);
                    
                })
                .then((item) => {
                    if (config.associatedForeignKeys) {
                        return resolve(models[config.modelName].findById(id, options));
                    }
                    return resolve(item);
                })
                .catch((err) => {
                    reject(err);
                });
        });
    },
    
    "delete": (id, config) => {
        return new Promise((resolve, reject) => {
            models[config.modelName]
                .findById(id)
                .then((item) => {
                    if (item === null) {
                        return resolve(null);
                    }
                    item.destroy();
                    resolve();
                })
                .catch((err) => {
                    reject(err);
                });
        });
    }
};
