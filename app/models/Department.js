"use strict";

module.exports = function(sequelize, DataTypes) {
    var Department = sequelize.define("Department", {
        name: DataTypes.TEXT,
    },{
        tableName: 'department',
        timestamps: true,
        createdAt: 'created_at',
        updatedAt: 'updated_at',
        classMethods: {
            associate: function(models) {
                Department.hasMany(models.Employee);
            }
        }
    
    });
    
    return Department;
};