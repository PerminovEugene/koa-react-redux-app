"use strict";

module.exports = function(sequelize, DataTypes) {
    var Employee = sequelize.define("Employee", {
        firstName: DataTypes.TEXT,
        lastName: DataTypes.TEXT
    },{
        tableName: 'employee',
        timestamps: true,
        createdAt: 'created_at',
        updatedAt: 'updated_at',
        classMethods: {
            associate: function(models) {
                Employee.belongsTo(models.Department, {
                    foreignKey: {
                        allowNull: true,
                    }
                });
            }
        }
    });
    
    return Employee;
};