const router = require('koa-route')
    , crudController = require('./../controllers/crud')
    , crudConfig = require('./../config/crud')
    , _ = require('lodash');



module.exports.addRoutes = (app) => {
    _.each(crudConfig.CrudRoutes, (routConfig, routUrl) => {
        app.use(router.put(`/${routUrl}`, crudController.create(routConfig)));
        app.use(router.get(`/${routUrl}/:id`, crudController.read(routConfig)));
        app.use(router.get(`/${routUrl}`, crudController.readAll(routConfig)));
        app.use(router.post(`/${routUrl}/:id`, crudController.update(routConfig)));
        app.use(router.delete(`/${routUrl}/:id`, crudController.delete(routConfig)));
    })
};