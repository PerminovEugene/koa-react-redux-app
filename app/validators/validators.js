const _ = require('lodash')
    , models = require('./../models');

const syncValidateMethods = {
    /**
     * function check required fields on existing
     * @param field {*} - field value
     * @param fieldName {string} - name of field for validation
     * @param params {booelean} true - it's required field
     * @return {string}
     */
    "required": (field, fieldName, params) => {
        if (params === true && !field) {
            return `Field is required.`
        }
    },
    "min": (field, fieldName, minimum) => {
        if (field && field.length < minimum ) {
            return `Minimal length ${minimum} symbols`
        }
    }
};


const asyncValidateMethods = {
    /**
     * function find in parent model instanse with current id
     * @param id {number} - id for find instance in parent model
     * @param fieldName {string] - name of field for validation
     * @param modelName {string} - name of model in orm
     * @return {Promise}
     */
    "foreignKeyFor": (id, fieldName, modelName) => {
        return new Promise((resolve, reject) => {
            models[modelName]
                .findById(id)
                .then((item) => {
                    if (item === null) {
                        let obj = {};
                        obj[fieldName] = `Not exist item for key ${fieldName} with id ${id}.`;
                        return resolve(obj);
                    }
                    return resolve();
                })
                .catch((err) => {
                    reject(err);
                });
        });
    }
};

/**
 * validate body via sync functions
 * @param body {object} - body from user request
 * @param config {object} - you can find expected format in config/crud.json
 * @return {{}} return object with errors if they exist, else undefined
 */
const validateWithSyncFunctions = (body, config) => {
    let errors = {};
    _.each(config, (validatorsObject, fieldName) => {
        const field = body[fieldName];
        let err, validationFunc;
        _.each(validatorsObject, (validationParam, validationType) => {
            validationFunc = syncValidateMethods[validationType];
            if (validationFunc) {
                err = validationFunc(field, fieldName, validationParam);
                if (err) {
                    errors[fieldName] = err;
                    return;
                }
            }
        });
    });
    return errors
};

/**
 * validate body via sync functions. These validation applying only for fields in body.
 * @param body {object} - body from user request
 * @param config {object} - you can find expected format in config/crud.json
 * @return {{}} return object with errors if they exist, else undefined
 */
const validateOnlyBodyWithSyncFunctions = (body, config) => {
    let errors = {};
    _.each(config, (validatorsObject, fieldName) => {
        if (body[fieldName] || body[fieldName] === '' || body[fieldName] === null) {
            const field = body[fieldName];
            let err, validationFunc;
            _.each(validatorsObject, (validationParam, validationType) => {
                validationFunc = syncValidateMethods[validationType];
                if (validationFunc) {
                    err = validationFunc(field, fieldName, validationParam);
                    if (err) {
                        errors[fieldName] = err;
                        return;
                    }
                }
            });
        }
    });
    return errors
};



/**
 * Validate body via async functions
 * @param body {object} - body from user request
 * @param config {object} - you can find expected format in config/crud.json
 * @return {Promise.<*>} return arrray with errors (because Promise.all)
 */
const validateWithAsyncFunctions = (body, config) => {
    let promises = [];
    _.each(config, (validatorsObject, fieldName) => {
        const field = body[fieldName];
        if (body[fieldName]) {
            _.each(validatorsObject, (validationParam, validationType) => {
                let asyncValidate = asyncValidateMethods[validationType];
                if (asyncValidate) {
                    promises.push(asyncValidateMethods[validationType](field, fieldName, validationParam));
                }
            });
        }
    });
    return Promise.all(promises);
};

const validate = (body, config, validateSync) => {
    return new Promise((resolve, reject) => {
        let errors = validateSync(body, config);
        if (_.keys(errors).length !== 0) {
            return resolve(errors);
        }
        
        validateWithAsyncFunctions(body, config)
            .then((result) => {
                let asyncErrorsArray = _.without(result, undefined);
                if (asyncErrorsArray.length > 0) {
                    return resolve(Object.assign({}, ...asyncErrorsArray))
                }
                return resolve();
            })
            .catch((err) => {
                console.log(err);
                reject(err);
            })
        
    });
};


module.exports = {
    /**
     * Functions for validate body by config. That's just wrappers for validate function with different params..
     * @param body {object} - body from user request
     * @param config {object} - you can find expected format in config/crud.json
     * @return {Promise} resolve errors if they exist, else undefined
     */
    validateAll: (body, config) => {
        return validate(body, config, validateWithSyncFunctions);
    },
    validateOnlyBody: (body, config) => {
        return validate(body, config, validateOnlyBodyWithSyncFunctions);
    },
};