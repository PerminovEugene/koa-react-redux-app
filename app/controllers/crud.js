const crudService = require('./../services/crud_service')
    , validators = require('./../validators/validators');

module.exports = {
    create: (routConfig) => {
        return (ctx) => {
            return new Promise((resolve, reject) => {
                validators
                    .validateAll(ctx.request.body, routConfig.validation)
                    .then((err) => {
                        if (err) {
                            ctx.body = {errors: err};
                            ctx.response.status = 400;
                            ctx.response.body = {errors: err};
                            return resolve();
                        }
                        crudService
                            .create(ctx.request.body, routConfig)
                            .then((item) => {
                                ctx.body = {data: item};
                                return resolve()
                            })
                    })
                    .catch((err) => {
                        console.log(err);
                        return reject();
                    });
            });
        }
    },
    
    read: (routConfig) => {
        return (ctx, id) => {
            return new Promise((resolve, reject) => {
                crudService
                    .read(id, routConfig)
                    .then((item) => {
                        ctx.body = {data: item};
                        return resolve();
                    })
                    .catch((err) => {
                        console.log(err);
                        return reject();
                    });
            });
        }
    },
    
    readAll: (routConfig) => {
        return (ctx) => {
            return new Promise((resolve, reject) => {
                crudService
                    .readAll(routConfig)
                    .then((items) => {
                        ctx.body = {data: items};
                        return resolve()
                    })
                    .catch((err) => {
                        console.log(err);
                        return reject();
                    });
            })
        }
    },
    
    update: (routConfig) => {
        return (ctx, id) => {
            return new Promise((resolve, reject) => {
                validators
                    .validateOnlyBody(ctx.request.body, routConfig.validation)
                    .then((err) => {
                        if (err) {
                            ctx.body = {errors: err};
                            ctx.response.status = 400;
                            ctx.response.body = {errors: err};
                            return resolve();
                        }
                        crudService
                            .update(id, ctx.request.body, routConfig)
                            .then((item) => {
                                if (item === null) {
                                    ctx.response.status = 404;
                                    return resolve();
                                }
                                ctx.body = {data: item};
                                return resolve();
                            })
                    })
                    .catch((err) => {
                        return reject(err);
                    });
            })
        }
    },
    
    "delete": (routConfig) => {
        return (ctx, id) => {
            return new Promise((resolve, reject) => {
                crudService
                    .delete(id, routConfig)
                    .then((result) => {
                        if (result === null) {
                            ctx.response.status = 404;
                            return resolve();
                        }
                        ctx.body = {success: true};
                        return resolve();
                    })
                    .catch((err) => {
                        return reject(err);
                    });
            })
        }
    }
}