const Koa = require('koa')
    , app = new Koa()
    , models = require('./app/models')
    , router = require('koa-router')()
    , crudRoutes = require('./app/routes/crud')
    , bodyParser = require('koa-body-parser')
    , cors = require('kcors');

models.sequelize.sync().then(function () {

    app.use(bodyParser());
    
    app.use(cors());
    
    crudRoutes.addRoutes(app);
    
    app.listen(3000);
});